#!/bin/bash

echo "Updating DNS resolvers..."
echo "nameserver 8.8.8.8" >> /etc/resolvconf/resolv.conf.d/head
echo "nameserver 8.8.4.4" >> /etc/resolvconf/resolv.conf.d/head

/sbin/resolvconf -u

echo "Installing OpenSSH and TightVNC..."
/usr/bin/apt-add-repository universe
/usr/bin/apt-add-repository multiverse
/usr/bin/apt update
/usr/bin/apt install -y openssh-server tightvncserver curl

echo "Assigning password to user ubuntu..."

echo -e "PASSWORD\nPASSWORD" | passwd ubuntu

echo "Downloading TeamViewer..."
/usr/bin/wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb
/usr/bin/apt install -y ./teamviewer_amd64.deb




